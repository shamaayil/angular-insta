import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthWrapperComponent } from './auth-wrapper/auth-wrapper.component';
import { LoginFormComponent } from './login-form/login-form.component';

const routes: Routes = [
  {path: '', component: AuthWrapperComponent, children: [
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {path: 'login', component: LoginFormComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
