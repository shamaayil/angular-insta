import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthWrapperComponent } from './auth-wrapper/auth-wrapper.component';
import { LoginFormComponent } from './login-form/login-form.component';

@NgModule({
  declarations: [AuthWrapperComponent, LoginFormComponent],
  imports: [
    CommonModule,
    AuthRoutingModule
  ]
})
export class AuthModule { }
